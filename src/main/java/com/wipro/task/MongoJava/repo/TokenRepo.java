package com.wipro.task.MongoJava.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * This interface is used to interact with the token document 
 * @author Sanal
 *
 */
import com.wipro.task.MongoJava.model.AuthToken;

public interface TokenRepo extends MongoRepository<AuthToken, String> {
	public AuthToken findOneByToken(String token);
}
