package com.wipro.task.MongoJava.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wipro.task.MongoJava.model.User;

/**
 * This interface is used to interact with the User document
 * 
 * @author Sanal
 *
 */
public interface UserRepo extends MongoRepository<User, String> {
	public User findOneByUserName(String userName);

	public User findOneByUserNameAndPassword(String userName, String password);

	public User findOneById(String id);
}
