package com.wipro.task.MongoJava.model;

/**
 * This is a model class for All exceptions
 * 
 * @author Sanal
 *
 */
public class ErrorResponse {
	private int statusCode;
	private String message;

	public ErrorResponse() {
		super();
	}

	public ErrorResponse(int code, String message) {
		super();
		this.statusCode = code;
		this.message = message;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int code) {
		this.statusCode = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
