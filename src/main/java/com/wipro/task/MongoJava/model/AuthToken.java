package com.wipro.task.MongoJava.model;

import java.util.Date;

import org.springframework.data.annotation.Id;

/**
 * This is a model class for Token
 * 
 * @author Sanal
 *
 */
public class AuthToken {
	@Id
	private String id;
	private String userId;
	private String token;
	private Date expiryDate;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
