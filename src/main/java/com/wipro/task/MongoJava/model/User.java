package com.wipro.task.MongoJava.model;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;

/**
 * This is the model class for a user
 * 
 * @author Sanal
 *
 */
public class User {
	@Id
	private String id;
	@NotNull
	private String userName;
	@NotNull
	private String password;
	private Address address;
	private String adharNumber;
	private String panNumber;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getAdharNumber() {
		return adharNumber;
	}

	public void setAdharNumber(String adharNumber) {
		this.adharNumber = adharNumber;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

}
