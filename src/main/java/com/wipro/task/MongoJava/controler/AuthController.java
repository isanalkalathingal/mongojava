package com.wipro.task.MongoJava.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.wipro.task.MongoJava.constants.Constants;
import com.wipro.task.MongoJava.model.AuthToken;
import com.wipro.task.MongoJava.model.LoginModel;
import com.wipro.task.MongoJava.model.ErrorResponse;
import com.wipro.task.MongoJava.model.User;
import com.wipro.task.MongoJava.repo.TokenRepo;
import com.wipro.task.MongoJava.repo.UserRepo;
import com.wipro.task.util.Util;

/**
 * This is a controller class for user login
 * 
 * @author Sanal
 *
 */
@RestController
@RequestMapping("/login")
public class AuthController {
	@Autowired
	UserRepo userRepository;
	@Autowired
	TokenRepo tokenRepository;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> login(@RequestBody LoginModel loginModel) {
		loginModel.setPassword(Util.getSecurePassword(loginModel.getPassword()));
		User user = userRepository.findOneByUserNameAndPassword(loginModel.getUserName(), loginModel.getPassword());
		if (user != null) {
			AuthToken token = new AuthToken();
			token.setToken(Util.generateToken(user.getId()));
			token.setUserId(user.getId());
			token.setExpiryDate(Util.getExpiryDate(30));
			tokenRepository.save(token);
			return new ResponseEntity<>(token, HttpStatus.OK);
		}
		ErrorResponse response = new ErrorResponse();
		response.setStatusCode(HttpStatus.OK.value());
		response.setMessage(Constants.LOGIN_VALIDATION);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

}
