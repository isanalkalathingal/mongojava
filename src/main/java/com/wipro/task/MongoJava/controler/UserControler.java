package com.wipro.task.MongoJava.controler;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.task.MongoJava.constants.Constants;
import com.wipro.task.MongoJava.model.AuthToken;
import com.wipro.task.MongoJava.model.ErrorResponse;
import com.wipro.task.MongoJava.model.User;
import com.wipro.task.MongoJava.repo.TokenRepo;
import com.wipro.task.MongoJava.repo.UserRepo;
import com.wipro.task.util.Util;

/**
 * This is a controller class for user
 * 
 * @author Sanal
 *
 */
@RestController
@RequestMapping("/user")
public class UserControler {

	@Autowired
	UserRepo userRepository;
	@Autowired
	TokenRepo tokenRepository;

	@RequestMapping(path = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> register(@Valid @RequestBody User user) {
		user.setPassword(Util.getSecurePassword(user.getPassword()));
		userRepository.save(user);
		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@RequestMapping(path = "/profile", method = RequestMethod.GET)
	public ResponseEntity<?> getUser(@RequestHeader("Authorization") String auth) {
		if (auth.isEmpty()) {
			String token = auth.replace("Token ", "");
			AuthToken authToken = tokenRepository.findOneByToken(token);
			if (authToken != null) {
				User user = userRepository.findOneById(authToken.getUserId());
				return new ResponseEntity<>(user, HttpStatus.OK);
			}
		}
		ErrorResponse response = new ErrorResponse();
		response.setStatusCode(HttpStatus.UNAUTHORIZED.value());
		response.setMessage(Constants.TOKEN_ERROR);
		return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
	}

}
