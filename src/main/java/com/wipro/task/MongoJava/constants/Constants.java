package com.wipro.task.MongoJava.constants;
/**
 * Application constant values 
 * @author Sanal
 *
 */
public class Constants {
  public static final String INTERNAL_SERVER_ERROR="Internal server error";
  public static final String LOGIN_VALIDATION="Invalid username or password";
  public static final String TOKEN_ERROR="Invalid or Expired token";
}
