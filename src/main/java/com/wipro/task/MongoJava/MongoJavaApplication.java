package com.wipro.task.MongoJava;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * This is the main application class 
 * @author Sanal
 *
 */
@SpringBootApplication()
public class MongoJavaApplication {
	public static void main(String[] args) {
		SpringApplication.run(MongoJavaApplication.class, args);
	}
}

