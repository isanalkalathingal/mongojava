package com.wipro.task.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
/**
 * This class is used for all utility methods for the application
 * @author Sanal
 *
 */
public class Util {
	 /**
	  * This method return an md5 string for a password
	  * @param passwordToHash
	  * @return
	  */
	 public static String getSecurePassword(String passwordToHash)
	    {
	        String generatedPassword = null;
	        try {
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            byte[] bytes = md.digest(passwordToHash.getBytes());
	            StringBuilder sb = new StringBuilder();
	            for(int i=0; i< bytes.length ;i++)
	            {
	                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	            }
	            //Get complete hashed password in hex format
	            generatedPassword = sb.toString();
	        }
	        catch (NoSuchAlgorithmException e) {
	            e.printStackTrace();
	        }
	        return generatedPassword;
	    }
	     /**
	      * This method generate a unique token for a user 
	      * @param id
	      * @return
	      */
	    public static String generateToken(String id) {
	    	SecureRandom random = new SecureRandom();
	    	byte bytes[] = new byte[20];
	    	random.nextBytes(bytes);
	    	return bytes.toString()+id;
	    }
	    /**
	     * This method return an expire date for a token 
	     * @param numberOfDays
	     * @return
	     */
	    public static Date getExpiryDate(int numberOfDays) {
	    	Date today=new Date();
	    	long ltime=today.getTime()+numberOfDays*24*60*60*1000;
	    	return new Date(ltime);
	    }
}
